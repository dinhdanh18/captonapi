const BASE_URL = "https://62bea99ebe8ba3a10d58a27a.mockapi.io/toDoList/AppleStoreProduct"
const productService = {
    getProduct: () => {
        return axios({
            url: BASE_URL,
            method: "GET"
        })
    }
}
const URL_GIOHANG = "https://62b0787ae460b79df0469bbc.mockapi.io/gio-hang"
const productGioHang = {
    postProduct: (product) => {
        return axios({
            url: URL_GIOHANG,
            method: "POST",
            data: product
        })
    }
}
const $ = document.querySelector.bind(document);

// button nav
document.getElementById('btn__nav').onclick = () => {
    $('NAV').classList.toggle("active");


}

const bagPurchaseBtn = $(".bag__btn")

// bagpruchase click
$('.overlay').onclick = () => {
    $('#cart').classList.remove('active');
}
bagPurchaseBtn.onclick = () => {
    $('#cart').classList.add('active');
}



$('#cancle').onclick = () => {
    $('#cart__purchase__wrapped').classList.add('hidden');
}

const renderProductList = (list) => {
    let HTML = ""
    for (var i = 0; i < list.length; i++) {
        let product = list[i];
        let itemProduct = `
        <div class="item text-white p-4" id="item-product" onclick="clickShowItem(${product.id})")>
        <div class="item__heading flex justify-between">
            <div class="LogoIcon"><i class="fa-brands fa-apple text-4xl text-white"></i></div>
            <div class="circleGb border border-inherit rounded-full p-2">
                <span class="GbNumber text-center">128 GB </span>
            </div>
        </div>
        <div class="item__content">
            <img src="${product.img}" alt="" class="product">
            <h1 class="item__name text-lg font-black text-center">
                ${product.name}
            </h1>

            <p class="title font-light text-md text-center">${product.description}</p>
        </div>
        <div class="controler flex justify-between pt-10">
            <div class="controler__btn flex items-center">
                <div class="controler__btn--minus">
                    <i class="fa-solid fa-minus"></i>
                </div>
                <span class="number px-2">0</span>
                <div class="controler__btn--plus">
                    <i class="fa-solid fa-plus"></i>
                </div>
            </div>
            <div class="controler__right flex items-center">
                <h2 id="price" class="text-xl mr-2">${product.price}$</h2>
                <div class="item__card--btn">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M16 11V7a4 4 0 00-8 0v4M5 9h14l1 12H4L5 9z" />
                    </svg>

                </div>
            </div>

        </div>
    </div>
        `
        HTML += itemProduct;
    }
    $('.product__right').innerHTML = HTML;
}


// danh
let product = {}
let productList = []
let showProduct = () => {
    productService.getProduct()
    .then((res) => {
        productList = res.data;
        renderProductList(res.data);
    })
    .catch((err) => {
        console.log(err);
    })
}
showProduct();
let clickShowItem = (id) => { 
    let index = productList.findIndex(item => item.id == id)
    product = productList[index];
    $('#product__left').innerHTML = `
    <div class="product__left__heading flex justify-between">
    <div class="LogoIcon"><i class="fa-brands fa-apple text-4xl text-white"></i></div>
    <div class="circleGb border border-inherit rounded-full p-2">
        <span class="GbNumber text-center">128 GB </span>
    </div>
</div>
<div class="product__left__content">
    <img src=${productList[index].img} alt="" class="product">
    <h1 class="product__name text-2xl font-black">
        ${productList[index].name}
    </h1>
    <div class="product__information py-2">
        <span class="product__information__item">Color: Midnight Blue </span>
        <span class="product__information__item">Camera:64px</span>

    </div>
    <p class="title font-light">${productList[index].description}</p>
</div>
<div class="controler flex justify-between pt-10">
    <div class="controler__btn flex items-center">
        <div class="controler__btn--minus">
            <i class="fa-solid fa-minus"></i>
        </div>
        <span class="number px-2">0</span>
        <div class="controler__btn--plus">
            <i class="fa-solid fa-plus"></i>
        </div>
    </div>
    <div class="controler__left flex items-center">
        <h2 id="price" class="text-xl mr-4">${productList[index].price} $</h2>
        <div class="add_to_cart"  onclick="addToCart(${productList[index].id})">
            <i class="fa-solid fa-bag-shopping pr-2"></i>
            <span" class="pl-1">Add to cart</span>
        </div>
    </div>

</div>`
}
let showItemGioHang = (list) => { 
    let contentGioHang = ''
    let total = 0
    let sum =0 
    for (let i = 0 ; i < list.length ; i++){
        let itemProduct = list[i].product
        total += itemProduct.price * itemProduct.quantity 
        sum += itemProduct.quantity;
        contentGioHang +=  `
        <div class="cart__item__select flex justify-between w-full text-white items-center">
        <img src=${itemProduct.img} alt="" class="cart__item--img">
        <p class="name__item">${itemProduct.name}</p>
        <div class="controler__btn flex items-center">
            <div class="controler__btn--minus" onclick="changeQuantity(${itemProduct.id},-1)">
                <i class="fa-solid fa-minus"></i>
            </div>
            <span class="number px-2">${itemProduct.quantity}</span>
            <div class="controler__btn--plus" onclick="changeQuantity(${itemProduct.id},1)">
                <i class="fa-solid fa-plus"></i>
            </div>
        </div>
        <p class="price">${itemProduct.price}$</p>
        <div class="btn__delete" onclick="deleteItem(${itemProduct.id})">
            <i class="fa-solid fa-trash"></i>
        </div>
        </div>
        `;
    }
    $("#total").innerHTML = `${total} $`
    $(".cart__container").innerHTML = contentGioHang
    $(".bag__btn-num").innerHTML = sum
 }
 
let listCart = []
let addToCart = (id) => {
    let index = listCart.findIndex(item => item.product.id == id)
    if(index !== -1){
        listCart[index].product.quantity++;
    }else{
        let cartItem =  {
            product: {...product, quantity: 1}
        }
        listCart.push(cartItem);
    }
    showItemGioHang(listCart)
    luuVaoLocal(listCart)
}
let changeQuantity = (id,step) => { 
    let index = listCart.findIndex(item => item.product.id == id)
    listCart[index].product.quantity += step;
    if(listCart[index].product.quantity < 1){
     listCart.splice(index,1);
    }
    showItemGioHang(listCart)
    luuVaoLocal(listCart)
}
let deleteItem = (id) => { 
    let index = listCart.findIndex(item => item.product.id == id)
    listCart.splice(index,1);
    showItemGioHang(listCart)
    luuVaoLocal(listCart)
 }
let deleteAllItem = () => {
    if(listCart.length <= 0 ){
        alert("Gio hang trong")
    }else{
        alert("Thanh Cong")
        listCart.splice(0,listCart.length)
        showItemGioHang(listCart)
        luuVaoLocal(listCart)
    }
    
}
function luuVaoLocal(list){
    listJSON = JSON.stringify(list);
    localStorage.setItem("listGioHang", listJSON);
}
let JSONdata = localStorage.getItem("listGioHang");
if(JSONdata !== null){
     listCart = JSON.parse(JSONdata);
    showItemGioHang(listCart)
}
let showTableThanhToan = (list) => { 
    let contentGioHang = ''
    let total = 0
    for (let i = 0 ; i < list.length ; i++){
        let productItem = list[i].product
        total += productItem.price * productItem.quantity 
        contentGioHang +=  `
        <tr>
        <td>${productItem.name}</td>
        <td>${productItem.quantity}</td>
        <td>${productItem.price}$</td>
         </tr>
        `;
     }
     $('#tbody_food').innerHTML = contentGioHang + 
     `<tr>
        <td></td>
        <td class="font-weight-bold">Total amount:</td>
        <td>${total}$</td>
    </tr>`;

}
let postProductGiohang = () => {
    let nameUser = $('#input__nameUser').value;
    listCart.map((item,i) => { 
        listCart[i].product = {...listCart[i].product,nameUser}
        productGioHang.postProduct(item.product)
    .then((res) => {
            console.log(res);
          })
          .catch((err) => {
            console.log(err);
          });
     })
}
const table = document.querySelector("#cart__purchase-table");
const purchaseNowBtn = document.querySelector(".purchase-now");
purchaseNowBtn.onclick = () => { 
    if($('#input__nameUser').value == ''){
        alert("Nhap ten khach hang")
    }else{
        postProductGiohang()
        deleteAllItem();
        $('#input__nameUser').value = '' 
        showTableThanhToan(listCart);
    }
}

$('#purchase').onclick = () => {
    $('#cart__purchase__wrapped').classList.remove('hidden');
    showTableThanhToan(listCart)
}


// <================================= swiper ================================>
const swiper = new Swiper('.swiper', {
    // Optional parameters
    // direction: 'vertical',
    loop: true,
    autoplay: {
        delay: 5000,
    },

    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
    },

    // Navigation arrows
    navigation: {
        // nextEl: '.swiper-button-next',
        // prevEl: '.swiper-button-prev',
    },

    // And if we need scrollbar
    scrollbar: {
        // el: '.swiper-scrollbar',
    },
});



