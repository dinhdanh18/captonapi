import { controlerListProduct } from "../controller/controller.js";
import { productService,cartService } from "./service/productService.js";


const add = document.getElementById('btn--add');
add.onclick = () => {
    document.getElementById('input__screen').classList.add("active");
}
const close = document.querySelector(".close");
close.onclick = () => {
    document.getElementById('input__screen').classList.remove("active");
    document.getElementById("update__product").classList.add("disabled");
}

function login() {
    let loginAccount = document.getElementById("login__account").value;
    let passAccount = document.getElementById("passWord__account").value;
    if (loginAccount == "TinThanh" && passAccount == "1") {
        document.getElementById('login__wrapped').classList.add("hidden");
        document.getElementById('list__manager').classList.remove("hidden");
    }
    else {
        document.getElementById('helpIdAccount').innerText = "Sai Tk hoac mat khau";
        document.getElementById('helpIdAccount').classList.remove("hidden");
    }
}
window.login = login;


let renderListProduct = (list) => {
    let HTML = ""
    for (var i = 0; i < list.length; i++) {
        let product = list[i];
        let contentTr = `
        <tr>
            <td>${product.id}</td>
            <td>
                <img src="${product.img}" style="max-width: 2rem;" alt="">
            </td>
            <td>${product.name}</td>
            <td>${product.price}</td>
            <td>${product.title}</td>
            <td>${product.description}</td>
            <td class="btn d-flex">
                <button onclick="editProduct('${product.id}')" class="btn btn-primary mr-2">Edit</button>
                <button class="btn btn-warning" onclick="deleteProduct('${product.id}')">Delete</button>
            </td>
        </tr>
        `
        HTML += contentTr;
    }
    document.getElementById('tbody__product').innerHTML = HTML;
    // console.log(HTML);
}
window.renderListProduct = renderListProduct;


// show list product
let showListProduct = () => {
    productService.getListProduct()
        .then((res) => {
            renderListProduct(res.data)

        })
        .catch((err) => {
            console.log(err);
        })
}

// delete product item
let deleteProduct = (idProduct) => {
    productService.deleteProduct(idProduct)
        .then((res) => {
            showListProduct();
        })
        .catch((err) => {
            console.log(err);
        })
}
// add product
let addProduct = () => {
    let product = controlerListProduct.getInforFromForm();
    productService.addProduct(product)
        .then((res) => {
            showListProduct();
            console.log("success");
        })
        .catch((err) => {
            console.log(err);
        })

}
let idProductEdited = null;
// edit product
let editProduct = (id) => {
    idProductEdited = id;

    productService.getDetailInforProduct(idProductEdited)
        .then((res) => {
            controlerListProduct.showInforProductFromList(res.data);
            document.getElementById("update__product").classList.remove("disabled");
        })
        .catch((err) => {
            console.log(err);
        })
    document.getElementById('input__screen').classList.add("active");
}

// update product
let updateProduct = () => {
    let product = controlerListProduct.getInforFromForm();
    let newProduct = { ...product, id: idProductEdited };

    productService.updateInforProduct(newProduct)
        .then((res) => {
            showListProduct();
            document.getElementById("update__product").classList.add("disabled");
            controlerListProduct.cleaFormInput();

        })
        .catch((err) => {
            console.log(err);
        })
}

//dinh danh
//render cart
let renderCart = (list) => {
    let contentHTML = ""
    for (var i = 0; i < list.length; i++) {
        let product = list[i];
        let contentTr = `
        <tr class="">
                            <td>${product.id}</td>
                            <td>
                                <img src=${product.src} style="max-width: 2rem;"  alt="">
                            </td>
                            <td>${product.nameUser}</td>
                            <td>${product.name}</td>
                            <td>${product.price}$</td>
                            <td>${product.quantity}</td>
                            <td class="btn d-flex">
                                <button class="btn btn-warning" onclick="deleteCart(${product.id})">Delete</button>
                            </td>
                        </tr>
        `
        contentHTML += contentTr;
    }
    document.getElementById('tbody__cart').innerHTML = contentHTML;
}

// GET CART
let showListCart= () => {
    cartService.getListCart()
    .then((res) => {
            console.log(res.data);
            renderCart(res.data)
            let today = new Date()
            console.log(today)
          })
          .catch((err) => {
            alert("Khong nhan duoc api")
        });
}
let deleteCart = (id) => { 
    cartService.deleteCart(id)
    .then((res) => {
            showListCart(res.data)
          })
          .catch((err) => {
            alert("That Bai")
          });
 }


window.deleteCart = deleteCart;
window.showListCart = showListCart;
window.updateProduct = updateProduct
window.editProduct = editProduct;
window.addProduct = addProduct;
window.deleteProduct = deleteProduct;
window.showListProdcut = showListProduct;
showListProduct();
showListCart()
